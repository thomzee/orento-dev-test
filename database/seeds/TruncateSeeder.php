<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TruncateSeeder extends Seeder
{
    private $exceptModels;
    public function __construct()
    {
        $this->exceptModels = [];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (scandir(app_path()) as $file) {
            if (is_file(app_path($file))) {
                $fileModel = str_replace('.php', '', $file);
                if (!in_array($fileModel, $this->exceptModels)) {
                    $classModel = '\\App\\'.$fileModel;
                    DB::statement('TRUNCATE TABLE ' . (new $classModel)->table . ' RESTART IDENTITY CASCADE;');
                }
            }
        }
    }
}
