<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 10)->create()->each(function ($u) {
            $count = rand(5,10);
            $preferred = rand(1, $count);
            for($i=1; $i<=$count;$i++) {
                $u->addresses()->save(factory(App\Address::class)->make(['preferred' => $i == $preferred ? true : false]));
            }
        });
    }
}
