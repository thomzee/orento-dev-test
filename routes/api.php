<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "Api" middleware group. Enjoy building your API!
|
*/

Route::group(['as' => 'api::', 'namespace' => 'Api', 'prefix' => 'v1'], function () {
    Route::post('auth/login', ['as' => 'auth.login', 'uses' => 'AuthController@login']);
    Route::get('auth/logout', ['as' => 'auth.logout', 'uses' => 'AuthController@logout'])->middleware('auth:api');;
    Route::post('auth/signup', ['as' => 'auth.signup', 'uses' => 'AuthController@signup']);
});

Route::group(['as' => 'api::', 'namespace' => 'Api', 'middleware' => 'auth:api', 'prefix' => 'v1'], function () {
    Route::get('address', ['as' => 'address.index', 'uses' => 'AddressController@index']);
    Route::post('address/create', ['as' => 'address.store', 'uses' => 'AddressController@store']);
    Route::get('address/show/{id}', ['as' => 'address.show', 'uses' => 'AddressController@show']);
    Route::put('address/edit/{id}', ['as' => 'address.update', 'uses' => 'AddressController@update']);
    Route::delete('address/delete/{id}', ['as' => 'address.destroy', 'uses' => 'AddressController@destroy']);
});