<?php

namespace Tests\Feature;

use Faker\Factory;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSignup()
    {
        $faker = Factory::create();

        $email = $faker->unique()->safeEmail;
        $username = explode('@', $email);

        $response = $this->json('POST', '/api/v1/auth/signup', [
            'username' => $username[0],
            'email' => $email,
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
            'address' => $faker->address
        ]);
        $response->assertStatus(201);
    }
}
