<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Libraries\ApiResponseLibrary;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AddressController extends ApiController
{
    private $userModel;
    private $apiResponseLibrary;

    public function __construct() {
        $this->userModel = new User();
        $this->apiResponseLibrary = new ApiResponseLibrary();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            return response($this->apiResponseLibrary->listPaginate($request->user()->addresses()));
        } catch (\Exception $e) {
            return response($this->apiResponseLibrary->errorResponse($e));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'detail' => 'required|max:255',
                'preferred' => 'required|boolean',
            ]);

            if ($validator->fails()) {
                return response($this->apiResponseLibrary->validationFailResponse($validator->errors()));
            }

            if ($request->preferred)
                $request->user()->addresses()->update([
                    'preferred' => false
                ]);

            $data = $request->user()->addresses()->create($request->all());

            return response($this->apiResponseLibrary->successResponse($data->id));
        } catch (\Exception $e) {
            return response($this->apiResponseLibrary->errorResponse($e));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        try {
            return response($this->apiResponseLibrary->singleData($request->user()->addresses()->findOrFail($id), []));
        } catch (\Exception $e) {
            return response($this->apiResponseLibrary->errorResponse($e));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'detail' => 'required|max:255',
                'preferred' => 'required|boolean',
            ]);

            if ($validator->fails()) {
                return response($this->apiResponseLibrary->validationFailResponse($validator->errors()));
            }

            if ($request->preferred)
                $request->user()->addresses()->where('id', '!=', $id)->update([
                    'preferred' => false
                ]);

            $request->user()->addresses()->findOrFail($id)->update($request->all());

            return response($this->apiResponseLibrary->successResponse($id));
        } catch (\Exception $e) {
            return response($this->apiResponseLibrary->errorResponse($e));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            $data = $request->user()->addresses()->findOrFail($id);
            if ($data->preferred) {
                $newPreferred = $request->user()->addresses()->where('id', '!=', $id)->first();
                if ($newPreferred) {
                    $newPreferred->preferred = true;
                    $newPreferred->update();
                }
            }
            $data->preferred = false;
            $data->update();
            $data->delete();

            return response($this->apiResponseLibrary->successResponse($id));
        } catch (\Exception $e) {
            return response($this->apiResponseLibrary->errorResponse($e));
        }
    }
}
