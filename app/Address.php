<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use SoftDeletes;

    public $table = 'addresses';

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = ['detail', 'preferred'];
}
